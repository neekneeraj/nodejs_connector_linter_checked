//node libs
/*global require, console */
var http = require('http');
var requestLib = require('request');
var Cookies = require('cookies');
//reading configs
var Config = require("./ss2_config");      //changed config to Config
//Object Config
var shieldsquare_config = new Config();
//reading ss2_part
var ssrequest = require("./ss2_part");
//Object config



//Data Holder Wrappers
//Request params

// Linter Corrected functions Starts
function dechex(dec) {
    "use strict";
    var str = parseInt(dec, 10).toString(16);
    return str;
}

function microtime(get_as_float) {
    "use strict";
    var now, s;
    now = new Date().getTime() / 1000;
    s = parseInt(now, 10);
    return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}

function shieldsquare_IP2Hex(ip) {
    "use strict";
    var hex = "", temp = "", part, i;
    try {
        part = ip.split('.');
        for (i = 0; i < part.length; i = i + 1) {
            temp = "0" + dechex(part[i]);
            temp =  temp.substr(temp.length - 2);
            hex += temp;
        }
    } catch (e) {
        hex = "ffffffff";
        return hex;
    }
    return hex;
}

function mt_rand(type) {
    "use strict";
	var arg_len = arguments.length, rnd_type = 0, min = 10000, max = 99999, min_hx = 4096, max_hx = 65535;
	if (arg_len === 1) {
		rnd_type = parseInt(type, 10);
	}
	if (rnd_type === 0) {
		return Math.floor(Math.random() * (max - min + 1) + min);
    } else {
		return Math.floor(Math.random() * (max_hx - min_hx + 1) + min_hx).toString(16);
    }
}

function uniqid() {
    "use strict";
	return (mt_rand(1) + mt_rand(1) + "-" + mt_rand(1) + "-" + mt_rand(1) + "-" + mt_rand(1) + "-" + mt_rand(1) + mt_rand(1) + mt_rand(1));
}

function shieldsquare_generate_pid(shieldsquare_sid, ipaddress) {
    "use strict";
    var t, arr;
	t = ("" + microtime(0)).split(" ");
	arr = shieldsquare_sid.split("-");
	return shieldsquare_IP2Hex(ipaddress) + "-" + arr[3] + "-"
        + ("00000000" + dechex(t[1])).substr(-4) + "-"
        + ("0000" + dechex(Math.round(t[0] * 65536))).substr(-4) + "-"
        + mt_rand(1) + mt_rand(1) + mt_rand(1);
}

// Linter Corrrected functions End

//Shieldsquare utility Function :

function shieldsquareUtility() {

    //request validate function
    this.shieldsquare_ValidateRequest = function (shieldsquare_username, shieldsquare_calltype, shieldsquare_pid, request, response, maincallback) {
        "use strict";
        var shieldsquare_low, shieldsquare_high, shieldsquare_a, shieldsquare_b, shieldsquare_c, shieldsquare_d, shieldsquare_e, shieldsquare_f, shieldsquare_time, shieldsquare_request, shieldsquare_RETURNCODES, shieldsquare_response, shieldsquare_curl_response, shieldsquare_service_url, request_timeout, ssresp, ipaddress, ipaddress_t, shieldsquare_ex_time, cookies, shieldsquare_lastaccesstime, shieldsquare_uzma, shieldsquare_uzmb, shieldsquare_uzmc, shieldsquare_uzmd, actualcount, readcount, expiryDate, fullUrl, shieldsquare_jsonObj, url, reqOptions, readSSResponse;
        
        shieldsquare_low = 10000;
        shieldsquare_high = 99999;
        shieldsquare_a = 1;
        shieldsquare_b = 3;
        shieldsquare_c = 7;
        shieldsquare_d = 1;
        shieldsquare_e = 5;
        shieldsquare_f = 10;
        shieldsquare_time = Math.floor(new Date() / 1000);

      //Instantiate Wrappers
        shieldsquare_request = new ssrequest.ShieldsquareRequest();
       // console.log(shieldsquare_request);
        shieldsquare_RETURNCODES = new ssrequest.ShieldsquareCodes();
        shieldsquare_response = new ssrequest.ShieldsquareResponse();
        shieldsquare_curl_response = new ssrequest.ShieldsquareResponseCode();
        shieldsquare_service_url = shieldsquare_config._ss2_domain;

      //Timeout and exceed condition
        request_timeout = shieldsquare_config._timeout_value;
        if (shieldsquare_config._timeout_value > 2000) {
            console.log("ShieldSquare Timeout cant be greater then 2000 milliseconds, limiting timeout to 2000ms");
            ssresp = {
                ssresp : -1,
                reason : "Timeout configured is higher than maximum limit (2000ms)"
            }; //Monitoring the traffic
            maincallback(ssresp);
            return -1;
        } else {
            request_timeout = shieldsquare_config._timeout_value;
        }

        ipaddress = "";
        ipaddress_t = "";
        
      //Reading ipaddr
        
        if (shieldsquare_config._ipaddr === "REMOTE_ADDR") {
            ipaddress_t = "" + request.connection.remoteAddress;
        } else {
            ipaddress_t = "" + request.headers[shieldsquare_config._ipaddr];
        }
        ipaddress_t = ("" + ipaddress_t).split(':');
        try {
            ipaddress = ipaddress_t[3];
        } catch (ex1) {
            if (ipaddress_t !== null) {
                ipaddress = ipaddress_t[0];
            }
        }
        if (ipaddress === undefined || ipaddress === "" || ipaddress === null) {
            ipaddress = request.ip;
            if (ipaddress === undefined || ipaddress === "" || ipaddress === null) {
                ipaddress = "0.0.0.0";
            }
        }

      //Generate the PID
        shieldsquare_pid = shieldsquare_generate_pid(shieldsquare_config._sid, ipaddress);

      //Cookies Set and Manipulation

      //Expire time : 10 years
        shieldsquare_ex_time = (shieldsquare_time + 3600 * 24 * 365 * 10) * 1000;

      //Instantiate Cookies Handler
        cookies = new Cookies(request, response);

      //Shieldsqure_time
        shieldsquare_lastaccesstime = shieldsquare_time;

      //get uuid cookie
        shieldsquare_uzma = cookies.get("__uzma");
        if (shieldsquare_uzma === undefined || shieldsquare_uzma === "") {
            shieldsquare_uzma = uniqid();
        }
      //Get set values of cookies (if any)
        shieldsquare_uzmb = cookies.get("__uzmb");
        if (shieldsquare_uzmb === undefined || shieldsquare_uzmb === "") {
        //Accessing page for the first time
            shieldsquare_uzmb = shieldsquare_lastaccesstime;
        }
        shieldsquare_uzmc = cookies.get("__uzmc");
        if (shieldsquare_uzmc === undefined || shieldsquare_uzmc === "") {
            actualcount = 1;
        } else {
            try {
                readcount = parseInt(shieldsquare_uzmc.substr(shieldsquare_e, shieldsquare_uzmc.length - shieldsquare_f), 10);
          //decoding count : -7 and /3
                actualcount = (readcount - shieldsquare_c) / parseInt(shieldsquare_b, 10);
                actualcount = actualcount + 1;
            } catch (ex2) {
                actualcount = 1;
            }
        }

      //encoding the value
        expiryDate = new Date(shieldsquare_ex_time);
        shieldsquare_uzmc = mt_rand(0) + "" + (actualcount * 3 + 7) + "" + mt_rand(0) + "";
    //  response.setHeader("Set-Cookie", ["__uzma=5638be2ba1ace9.67752411; expires="+ date +"; path=/"]);
      //Set all cookies

        response.setHeader('Set-Cookie',
             ['__uzma=' + shieldsquare_uzma + ";expires=" + expiryDate + "; path=/",
                '__uzmb=' + shieldsquare_uzmb + ";expires=" + expiryDate + "; path=/",
                '__uzmc=' + shieldsquare_uzmc + ";expires=" + expiryDate + "; path=/",
                '__uzmd=' + shieldsquare_lastaccesstime + ";expires=" + expiryDate + "; path=/"
                ]);

      //get request values for cookies
        shieldsquare_request.__uzma = shieldsquare_uzma;
        shieldsquare_request.__uzmb = shieldsquare_uzmb;
        shieldsquare_request.__uzmc = shieldsquare_uzmc;
        shieldsquare_request.__uzmd = shieldsquare_lastaccesstime;

      //Mode
        shieldsquare_request._zpsbd0 = false;
        if (shieldsquare_config._mode === "Active") {
            shieldsquare_request._zpsbd0 = true;
        }

      //Subscriber ID
        shieldsquare_request._zpsbd1 = shieldsquare_config._sid;
      //PID Value
        shieldsquare_request._zpsbd2 = shieldsquare_pid;
      //http/s reffer
        shieldsquare_request._zpsbd3 = (request.headers.referer !== undefined) ? request.headers.referer : '';
      //request url
        fullUrl = request.protocol + '://' + request.get('host') + request.originalUrl;
        shieldsquare_request._zpsbd4 = fullUrl;
      //get session ID
        shieldsquare_request._zpsbd5 = cookies.get(shieldsquare_config._sessid) !== undefined ? cookies.get(shieldsquare_config._sessid) : '';
      //get IP Address
        shieldsquare_request._zpsbd6 = ipaddress;
      //get UserAgent
        shieldsquare_request._zpsbd7 = (request.headers['user-agent'] !== undefined) ? request.headers['user-agent'] : '';
      //Calltype
        shieldsquare_request._zpsbd8 = shieldsquare_calltype;
      //User Name
        shieldsquare_request._zpsbd9 = shieldsquare_username;
      //current timestamp
        shieldsquare_request._zpsbda = shieldsquare_lastaccesstime;

        shieldsquare_jsonObj = shieldsquare_request;
        console.log(shieldsquare_jsonObj);

        url = "http://" + shieldsquare_config._ss2_domain + "/getRequestData";
        reqOptions = {
            uri: url,
            method: "POST",
            headers: {"Content-Type" : "application/json"},
            timeout: shieldsquare_config._timeout_value,
            body: encodeURIComponent(JSON.stringify(shieldsquare_jsonObj)),
            forever: true //keepalive
        };
        if (shieldsquare_config.hasOwnProperty('httpProxy')) {
            reqOptions.proxy = shieldsquare_config.httpProxy;
        }

      //Mode = Monitor
        if (!shieldsquare_request._zpsbd0 === true) {
        //Mode : Monitor
        //Async POST and forget
            requestLib(reqOptions, function (error, response, body) {
                if (shieldsquare_config.debugFlag === true) {
                    if (!error) {
                        console.log("server reply, body (monitor mode):" + body);
                    }
                }
            });

            ssresp = {
                ssresp: 1
            }; //Monitoring the traffic
            maincallback(ssresp);
        } else {
        //Mode : Active
        //Sync Post and Read Response

      
     // var readSSResponse;

            reqOptions.noDelay = true;
      

            requestLib(reqOptions, function (error, response, body) {
                if (!error) {
                    if (shieldsquare_config.debugFlag === true) {
                        console.log("server reply, body (active mode):" + body);
                    }
                }
            }).on('response', function (response) {
    // unmodified http.IncomingMessage object
                response.on('data', function (data) {
      // compressed data as it is received
                    if (shieldsquare_config.debugFlag === true) {
                        console.log('received ' + data + ' bytes of compressed data');
                    }
                    readSSResponse = data;
                    readSSResponse = JSON.parse(readSSResponse);
                    maincallback(readSSResponse);
                });
            });
      
        }
        ssresp = {
            'pid' : shieldsquare_request._zpsbd2
        };
        if (shieldsquare_config.debugFlag === true) {
            console.log("pid ext = smembers S:1181:" + shieldsquare_request._zpsbd2);
        }
        return ssresp;
    };

    this.shieldsquare_IP2Hex = function (ip) {
        var hex, part, i;
        hex = "";
        part = ip.split('.');
        for (i = 0; i < part.length; i = i + 1) {
            hex += dechex(part[i]);
        }
        return hex;
    };
}

//Helper Utils

function hexdec(hex) {
    "use strict";
    return parseInt(hex, 16);
}


// A simple Random Number which will return a 5 digit intiger and the current
// system time is considered as Random Seed.

// Time based UID Generation Function.



module.exports = shieldsquareUtility;
