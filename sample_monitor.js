var http = require('http');
var util = require("./ss2.js");  //Path to ss2.js
var shieldsquare_util = new util();
var session=require('express-session');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(session({secret:'anysecureidentity'}));

app.get('/', function (request, response) {
    
    app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
    
    //This can be any cookie used to track sessions for end users
	request.session.SESSID='mysession-tracking-id-for-shieldsquare';

	$shieldsquare_userid = ""; // Enter the UserID of the user (only when blacklisting of logged-in users is required)
	$shieldsquare_calltype = 1;
    try {
        var $shieldsquare_pid = shieldsquare_util.shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid, request, response, function(shieldsquare_response){
            if(shieldsquare_response.ssresp == 1) {
                //console.log("Monitoring Traffic!");  <- For debug purpose
            }
        });
    } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
    }
   try {
        var renPid = '';
        if($shieldsquare_pid==undefined) {
            renPid = '';
        } else {
          renPid = $shieldsquare_pid.pid==undefined?'':$shieldsquare_pid.pid   
          }
      } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
    }
    try {
        response.render('index',{
		pid: renPid,
		});
        } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
    }
}).on('error',function(err) {});

process.on('uncaughtException', function (err) {});

app.listen(3000);
console.log("App listening 3000 at 127.0.0.1");
