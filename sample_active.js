var express = require('express');
var bodyParser = require('body-parser');
var util = require("./ss2.js");
var session=require('express-session');

var app = express();

		app.use(bodyParser.urlencoded({
		  extended: true
		}));

		app.use(session({secret:'anysecureidentity'}));

app.get('/', function(req,res,next){
	//to test hit the "/test" url!
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');

	req.session.SESSID='mysession-tracking-id-for-shieldsquare';

	shieldsquare_util = new util();

	$shieldsquare_userid = ""; // Enter the UserID of the user (only when blacklisting of logged-in users is required)
	$shieldsquare_calltype = 1;
	//It will be like blocking call = Make sure ss2_config MODE says "Active"
    try {
        var $shieldsquare_pid = shieldsquare_util.shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid, req, res,function(shieldsquare_response){
            //Got output here
            if (shieldsquare_response.ssresp == 0)
                console.log("Allow the user request");
            else if (shieldsquare_response.ssresp == 1)
                console.log("Monitor this Traffic");
            else if (shieldsquare_response.ssresp == 2)
                console.log("Show CAPTCHA before displaying the content");
            else if (shieldsquare_response.ssresp == 3)
                console.log("Block This request");
            else if (shieldsquare_response.ssresp == 4)
                console.log("Feed Fake Data");
            else if (shieldsquare_response.ssresp == -1 || shieldsquare_response.ssresp==undefined || shieldsquare_response.ssresp==null)
            {
                console.log("Error - " + shieldsquare_response.reason + "<BR>");
                console.log("Please reach out to ShieldSquare support team for assistance <BR>");
                console.log("Allow the user request");
            }
        });
    } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
    }
     
 try {
        var renPid = '';
        if($shieldsquare_pid==undefined) {
            renPid = '';
        } else {
          renPid = $shieldsquare_pid.pid==undefined?'':$shieldsquare_pid.pid   
          }
      } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
    }
  try{
	res.render('index',{
		pid: renPid,
		});
     } catch(ex) {
        console.log("Shieldsquare Exception => Message : "+ex);
       }
});

process.on('uncaughtException', function (err) {});

app.listen(3000);
console.log("App listening 3000");
