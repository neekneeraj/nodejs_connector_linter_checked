exports.ShieldsquareRequest = function () {
    //"use strict";
   // var _zpsbd0, _zpsbd1, _zpsbd2, _zpsbd3, _zpsbd4, _zpsbd5, _zpsbd6, _zpsbd7, _zpsbd8, _zpsbd9, _zpsbda, __uzma, __uzmb, __uzmc, __uzmd;
    this._zpsbd0 = false;
    this._zpsbd1 = "";
    this._zpsbd2 = "";
    this._zpsbd3 = "";
    this._zpsbd4 = "";
    this._zpsbd5 = "";
    this._zpsbd6 = "";
    this._zpsbd7 = "";
    this._zpsbd8 = "";
    this._zpsbd9 = "";
    this._zpsbda = "";
    this.__uzma = "";
    this.__uzmb = 0;
    this.__uzmc = "";
    this.__uzmd = 0;
};

//Active mode : for response based action
exports.ShieldsquareResponseCode = function () {
    this.error_string = "";
    this.responsecode = 0;
};

//JSON Packet as Response from shieldsquare
exports.ShieldsquareResponse = function () {
    this.pid = "";
    this.responsecode;
    this.url = "";
    this.reason = "";
};

exports.ShieldsquareCodes = function () {
    this.ALLOW = 0;
    this.MONITOR = 1;
    this.CAPTCHA = 2;
    this.BLOCK = 3;
    this.ALLOW_EXP = -1;
};
