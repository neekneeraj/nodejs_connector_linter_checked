function shieldsquare_config()
{
        /*
         *  Enter your SID .
        */
        this._sid = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
        

        /*

         * Please specify the mode in which you want to operate
         *
         * public $_mode = "Active";
         * or
         * public $_mode = "Monitor";
         */
        this._mode = "Monitor";


        /*
         * Timeout in Milliseconds : SHOULD be less than or equal to 2000ms
         */
        this._timeout_value = 500;
        
        /*
         * SESSID will be the default session-id for NodeApps for ShieldSquare Tracking the session.
         * So Maintain the convection and set the session id in request.SESSID only
        */
        this._sessid = 'connect.sid';

        /*
         * Set the ShieldSquare domain based on your Server Locations
         *    Asia/India     -  'ss_sa.shieldsquare.net'
         *    Europe         -  'ss_ew.shieldsquare.net'
         *    Australia      -  'ss_au.shieldsquare.net'
         *    South America  -  'ss_br.shieldsquare.net'
         *    North America  -  'ss_scus.shieldsquare.net'
         */

        this._ss2_domain = 'ss_scus.shieldsquare.net';

        /*
         * Parameter that holds the value of IPAddress.
         * Default is REMOTE_ADDR
         *
        */

        this._ipaddr = 'REMOTE_ADDR';

        this.debugFlag = false; // for internal use, set true for debug mode. CAUTION: do not set to 'true' in production setting

        /*
         * Proxy configuration url. Comment the line if you are not using proxy for outbound connections
        */
        //this.httpProxy = "http://10.10.10.10:80";
}

module.exports = shieldsquare_config;
